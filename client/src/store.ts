import { persistentAtom } from '@nanostores/persistent'

export const locale = persistentAtom<string>('locale', 'en')