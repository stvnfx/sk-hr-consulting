import { locale } from '../store';
import '../services/endpoint';
import { useEffect, useState } from 'react';

const LanguageSwitcher = () => {
    enum Language {
        NL = 'nl',
        EN = 'en',
    }

    const [currentLang, setCurrent] = useState("")

    const setLang = (lang: string) => {
        setCurrent(lang)
        locale.set(lang)
    }

    const redirect = (url: string) => {
        window.location.href = url
    }

    const changeLanguage = (adjust: string) => {
        if(adjust === Language.NL) {
            setLang(Language.NL)
            redirect('/')
        }
        if(adjust === Language.EN) {
            setLang(Language.EN)
            redirect('/en')
        }
    }

    useEffect(() => {
        setCurrent(locale.get())
    }, [])

    return (
        <div className='pr-4'>
            <button 
                className={currentLang === Language.NL ? "font-semibold" : ""}
                onClick={() => changeLanguage(Language.NL)}
            >
                nl
            </button> 
            <span className='mx-2'>
                |
            </span>
            <button
                className={currentLang === Language.EN ? "font-semibold" : ""}
                onClick={() => changeLanguage(Language.EN)}
            >
                en
            </button>
        </div>
    )
}

export default LanguageSwitcher