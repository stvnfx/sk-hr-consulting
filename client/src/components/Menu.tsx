import { useState } from "react"
import { motion, Variants } from 'framer-motion'
import { locale } from '../store';

const Menu = () => {
    const [showMenu, setShowMenu] = useState(false)
    const language = locale.get()

    const toggleMenu = () => {
        setShowMenu(showMenu => !showMenu)
    }

    const menuBlock = {
        hidden: { opacity: 0 },
        visible: {
          opacity: 1,
          transition: {
            duration: 0.1
          }
        }
    }

    const nlMenuItems = [
        {
            label: 'Home',
            url: '/'
        },
        {
            label: 'Over mij',
            url: '#about-me'
        },
        {
            label: 'Contact',
            url: '#contact'
        },
    ]

    const enMenuItems = [
        {
            label: 'Home',
            url: '/en'
        },
        {
            label: 'About me',
            url: '#about-me'
        },
        {
            label: 'Contact',
            url: '#contact'
        },
    ]

    const listItems = () => {
        if(language === 'nl') {
            return nlMenuItems.map((item, ind) => {
                return <motion.a animate={{ opacity: 1, y: 0 }} initial={{ opacity: 0, y: 20 }} transition={{ delay: 0.5 * ind }} href={item.url} key={`${item.label}-${ind}`} className="block text-8xl text-end mb-6 hover:text-primary transition-colors duration-100" onClick={() => toggleMenu()}>
                    {item.label}
                </motion.a>
            })
        } else {
            return enMenuItems.map((item, ind) => {
                return <motion.a animate={{ opacity: 1, y: 0 }} initial={{ opacity: 0, y: 20 }} transition={{ delay: 0.5 * ind }} href={item.url} key={`${item.label}-${ind}`} className="block text-8xl text-end mb-6 hover:text-primary transition-colors duration-100" onClick={() => toggleMenu()}>
                    {item.label}
                </motion.a>
            })
        }
    }

    return (
        <section className="fixed ml-16 z-20">
            <button 
                className="bg-brown hover:bg-neutral-50 text-neutral-50 p-2 group rounded-full shadow-md transition-colors"
                onClick={toggleMenu}
            >
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="group-hover:stroke-brown transition-colors w-6 h-6"
                >
                    <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M3.75 9h16.5m-16.5 6.75h16.5"
                    />
                </svg>
            </button>
            {showMenu && (
                <motion.div animate="visible" variants={menuBlock} initial="hidden" className="fixed top-0 right-0 bottom-0 left-0 bg-brown text-black pb-6">
                    <div className="container flex items-center justify-between">
                        <a href="/" className="opacity-0" aria-label="Click here to visit the homepage">
                            <img src="/logo.svg" alt="" role="presentation" height="150px" width="150px"/>
                        </a>
                        <div className="flex items-center">
                            <button 
                                className="bg-primary hover:bg-neutral-50 p-2 translate-x-10 group rounded-full transition-colors"
                                onClick={toggleMenu}
                            >
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth={1.5}
                                    stroke="currentColor"
                                    className="group-hover:stroke-black transition-colors w-6 h-6"
                                >
                                    <path 
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M6 18L18 6M6 6l12 12"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div className="container">
                        {listItems()}
                    </div>
                </motion.div>
            )}
        </section>
    )
}

export default Menu