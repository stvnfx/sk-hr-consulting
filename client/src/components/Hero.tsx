import { useState } from "react"
import { motion } from 'framer-motion'

const Hero = ({ description }) => {

    return (
        <div className="w-full lg:w-2/3">
            <motion.div
                className="font-light text-2xl"
                animate={{ opacity: 1, y: 0 }}
                initial={{ opacity: 0, y: 60 }}
                transition={{ delay: 0.5, ease: 'easeInOut' }}
                dangerouslySetInnerHTML={{__html: description}} 
            />
        </div>
    )
}

export default Hero