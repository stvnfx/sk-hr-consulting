import { defineConfig } from 'astro/config';
import { loadEnv } from 'vite';

import storyblok from '@storyblok/astro';

// https://astro.build/config
import tailwind from "@astrojs/tailwind";

// https://astro.build/config
import react from "@astrojs/react";

// https://astro.build/config
import vue from "@astrojs/vue";

// https://astro.build/config
import vercel from "@astrojs/vercel/serverless";

const env = loadEnv("", process.cwd(), 'STORYBLOK');

// https://astro.build/config
export default defineConfig({
  output: "server",
  adapter: vercel(),
  integrations: [
    tailwind(),
    react(),
    vue(),
    storyblok({
      accessToken: env.STORYBLOK_TOKEN,
      components: {
        hero: 'storyblok/Hero',
        feature: 'storyblok/Feature',
        grid: 'storyblok/Grid',
        page: 'storyblok/Page',
        teaser: 'storyblok/Teaser',
      }
    })
  ],
  experimental: {
    assets: true
  }
});