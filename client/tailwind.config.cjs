/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
	content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
	theme: {
		container: {
			center: true,
			padding: '4rem',
		},
		extend: {
			colors: {
				pink: {
					DEFAULT: "#E2B9B7",
				},
				brown: {
					DEFAULT: "#B1898C",
				},
				brique: {
					DEFAULT: "#C1985A",
				},
				beige: {
					DEFAULT: "#D4CBBD",
				},
				primary: {
					DEFAULT: "#DCC4B8"
				}
			},
			fontFamily: {
				sans: ["'Rasa'", ...defaultTheme.fontFamily.sans],
			},
			animation: {
				'text-scroll': 'text-scroll 20s linear infinite'
			},
			keyframes: {
				'text-scroll': {
					'0%': { transform: 'translateX(100%)'},
					'100%': { transform: 'translateX(-110%)'}
				}
			}
		}
	},
	plugins: [],
}
